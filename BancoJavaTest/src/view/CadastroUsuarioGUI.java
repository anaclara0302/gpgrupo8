package view;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import model.ModeloTabelaCliente;
import model.ModeloTabelaUsuario;
import dao.TabelaClienteDAO;
import dao.TabelaUsuarioDAO;

import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPasswordField;

public class CadastroUsuarioGUI extends JFrame {
 private JPanel contentPane;
 private JTextField txtNomeUsuario;
 private JTextField txtTelefone;
 private JTextField txtEmail;
 private JTextField txtNacionalidade;
 private JTextField txtUsername;
 private JPasswordField password;
 /**
  * Launch the application.
  */
 public static void main(String[] args) {
  EventQueue.invokeLater(new Runnable() {
   public void run() {
    try {
     CadastroUsuarioGUI frame = new CadastroUsuarioGUI();
     frame.setVisible(true);
    } catch (Exception e) {
     e.printStackTrace();
    }
   }
  });
 }
 /**
  * Create the frame.
  */
 public CadastroUsuarioGUI() {
  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  setBounds(100, 100, 526, 344);
  contentPane = new JPanel();
  contentPane.setForeground(new Color(255, 69, 0));
  contentPane.setBackground(Color.BLACK);
  contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
  setContentPane(contentPane);
  contentPane.setLayout(null);
  
  JLabel lblNome = new JLabel("Nome:");
  lblNome.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblNome.setForeground(new Color(255, 69, 0));
  lblNome.setBounds(10, 78, 46, 14);
  contentPane.add(lblNome);
  
  txtNomeUsuario = new JTextField();
  txtNomeUsuario.setBounds(68, 76, 155, 20);
  contentPane.add(txtNomeUsuario);
  txtNomeUsuario.setColumns(10);
  
  JLabel lblTelefone = new JLabel("Telefone:");
  lblTelefone.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblTelefone.setForeground(new Color(255, 69, 0));
  lblTelefone.setBounds(292, 78, 70, 14);
  contentPane.add(lblTelefone);
  
  txtTelefone = new JTextField();
  txtTelefone.setBounds(365, 76, 116, 20);
  contentPane.add(txtTelefone);
  txtTelefone.setColumns(10);
  
  JLabel lblEmail = new JLabel("Email:");
  lblEmail.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblEmail.setForeground(new Color(255, 69, 0));
  lblEmail.setBounds(10, 112, 46, 14);
  contentPane.add(lblEmail);
  
  txtEmail = new JTextField();
  txtEmail.setBounds(68, 110, 155, 20);
  contentPane.add(txtEmail);
  txtEmail.setColumns(10);
  
  JLabel lblNacionalidade = new JLabel("Nacionalidade:");
  lblNacionalidade.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblNacionalidade.setForeground(new Color(255, 69, 0));
  lblNacionalidade.setBounds(261, 112, 100, 14);
  contentPane.add(lblNacionalidade);
  
  txtNacionalidade = new JTextField();
  txtNacionalidade.setBounds(365, 110, 116, 20);
  contentPane.add(txtNacionalidade);
  txtNacionalidade.setColumns(10);
  
  JLabel lblNomeDeUsurio = new JLabel("Nome de Usu\u00E1rio:");
  lblNomeDeUsurio.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblNomeDeUsurio.setForeground(new Color(255, 69, 0));
  lblNomeDeUsurio.setBounds(43, 180, 116, 14);
  contentPane.add(lblNomeDeUsurio);
  
  txtUsername = new JTextField();
  txtUsername.setBounds(169, 178, 162, 20);
  contentPane.add(txtUsername);
  txtUsername.setColumns(10);
  
  JLabel lblSenha = new JLabel("Senha:");
  lblSenha.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblSenha.setForeground(new Color(255, 69, 0));
  lblSenha.setBounds(115, 211, 46, 14);
  contentPane.add(lblSenha);
  
  JButton btnCadastrar = new JButton("Cadastrar");
  btnCadastrar.addActionListener(new ActionListener() {
  	public void actionPerformed(ActionEvent arg0) {
  		
  		/* recupera os dados da interface gráfica */
		String nomeUsuario = txtNomeUsuario.getText();
		String telefone = txtTelefone.getText();
		String email = txtEmail.getText();
		String nacionalidade = txtNacionalidade.getText();
		String username = txtUsername.getText() ;
		String password ;


		/* cria um objeto para encapsular os campos */
		ModeloTabelaUsuario usuario = new ModeloTabelaUsuario(nomeUsuario, telefone, email, nacionalidade);


		/* cria o objeto de dao */
		TabelaUsuarioDAO daoUsuario = new TabelaUsuarioDAO();

		if(daoUsuario.Inserir(usuario)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		
		}}
  	
  });
  btnCadastrar.setBounds(381, 240, 100, 36);
  contentPane.add(btnCadastrar);
  
  password = new JPasswordField();
  password.setBounds(169, 209, 162, 20);
  contentPane.add(password);
  
  JLabel lblCadastro = new JLabel("Cadastro");
  lblCadastro.setFont(new Font("Tahoma", Font.BOLD, 16));
  lblCadastro.setForeground(SystemColor.activeCaption);
  lblCadastro.setBounds(10, 11, 116, 28);
  contentPane.add(lblCadastro);
 }
}