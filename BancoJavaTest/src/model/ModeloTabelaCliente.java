package model;

public class ModeloTabelaCliente {
	
	private String nomeCliente;
	private String enderecoCliente;
	private int idadeCliente;

	
	public ModeloTabelaCliente() {
		super();
	}

	public ModeloTabelaCliente(String nomeCliente, String enderecoCliente, int idadeCliente) {
		super();
		this.nomeCliente = nomeCliente;
		this.enderecoCliente = enderecoCliente;
		this.idadeCliente = idadeCliente;
	}
	
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getEnderecoCliente() {
		return enderecoCliente;
	}
	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}
	public int getIdadeCliente() {
		return idadeCliente;
	}
	public void setIdadeCliente(int idadeCliente) {
		this.idadeCliente = idadeCliente;
	}
	
	

}
