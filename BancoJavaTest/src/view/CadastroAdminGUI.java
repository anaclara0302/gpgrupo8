package view;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Font;

public class CadastroAdminGUI extends JFrame {
 private JPanel contentPane;
 private JTextField textField;
 private JTextField textField_1;
 private JTextField textField_2;
 private JTextField textField_3;
 private JTextField textField_4;
 private JTextField textField_5;
 /**
  * Launch the application.
  */
 public static void main(String[] args) {
  EventQueue.invokeLater(new Runnable() {
   public void run() {
    try {
     CadastroUsuarioGUI frame = new CadastroUsuarioGUI();
     frame.setVisible(true);
    } catch (Exception e) {
     e.printStackTrace();
    }
   }
  });
 }
 /**
  * Create the frame.
  */
 public CadastroAdminGUI() {
  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  setBounds(100, 100, 499, 316);
  contentPane = new JPanel();
  contentPane.setForeground(new Color(0, 0, 0));
  contentPane.setBackground(new Color(248, 248, 255));
  contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
  setContentPane(contentPane);
  contentPane.setLayout(null);
  
  JLabel lblNome = new JLabel("Nome:");
  lblNome.setBackground(new Color(248, 248, 255));
  lblNome.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblNome.setForeground(new Color(0, 0, 0));
  lblNome.setBounds(1, 88, 46, 14);
  contentPane.add(lblNome);
  
  textField = new JTextField();
  textField.setBounds(97, 86, 363, 20);
  contentPane.add(textField);
  textField.setColumns(10);
  
  JLabel lblTelefone = new JLabel("Telefone:");
  lblTelefone.setBackground(new Color(248, 248, 255));
  lblTelefone.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblTelefone.setForeground(new Color(0, 0, 0));
  lblTelefone.setBounds(1, 158, 59, 14);
  contentPane.add(lblTelefone);
  
  textField_1 = new JTextField();
  textField_1.setBounds(97, 156, 125, 20);
  contentPane.add(textField_1);
  textField_1.setColumns(10);
  
  JLabel lblEmail = new JLabel("Email:");
  lblEmail.setBackground(new Color(248, 248, 255));
  lblEmail.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblEmail.setForeground(new Color(0, 0, 0));
  lblEmail.setBounds(1, 123, 46, 14);
  contentPane.add(lblEmail);
  
  textField_2 = new JTextField();
  textField_2.setBounds(97, 121, 363, 20);
  contentPane.add(textField_2);
  textField_2.setColumns(10);
  
  JLabel lblNacionalidade = new JLabel("Nacionalidade:");
  lblNacionalidade.setBackground(new Color(248, 248, 255));
  lblNacionalidade.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblNacionalidade.setForeground(new Color(0, 0, 0));
  lblNacionalidade.setBounds(245, 156, 100, 14);
  contentPane.add(lblNacionalidade);
  
  textField_3 = new JTextField();
  textField_3.setBounds(344, 156, 116, 20);
  contentPane.add(textField_3);
  textField_3.setColumns(10);
  
  JLabel lblNomeDeUsurio = new JLabel("Identifica\u00E7\u00E3o :");
  lblNomeDeUsurio.setBackground(new Color(248, 248, 255));
  lblNomeDeUsurio.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblNomeDeUsurio.setForeground(new Color(0, 0, 0));
  lblNomeDeUsurio.setBounds(1, 190, 116, 14);
  contentPane.add(lblNomeDeUsurio);
  
  textField_4 = new JTextField();
  textField_4.setBounds(97, 188, 192, 20);
  contentPane.add(textField_4);
  textField_4.setColumns(10);
  
  JLabel lblSenha = new JLabel("Senha:");
  lblSenha.setBackground(new Color(248, 248, 255));
  lblSenha.setFont(new Font("Yu Gothic UI", Font.BOLD, 13));
  lblSenha.setForeground(new Color(0, 0, 0));
  lblSenha.setBounds(1, 221, 46, 14);
  contentPane.add(lblSenha);
  
  textField_5 = new JTextField();
  textField_5.setBounds(97, 219, 192, 20);
  contentPane.add(textField_5);
  textField_5.setColumns(10);
  
  JButton btnCadastrar = new JButton("Cadastrar");
  btnCadastrar.setBounds(371, 211, 89, 36);
  contentPane.add(btnCadastrar);
  
  JLabel lblCadastroAdministrador = new JLabel("Cadastro administrador ");
  lblCadastroAdministrador.setFont(new Font("Tahoma", Font.PLAIN, 20));
  lblCadastroAdministrador.setBounds(14, 11, 288, 36);
  contentPane.add(lblCadastroAdministrador);
 }
}