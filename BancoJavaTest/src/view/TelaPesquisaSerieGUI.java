package view;
//OI
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;

import javax.swing.JLabel;

import model.ModeloTabelaEpisodio;
import model.ModeloTabelaSerie;
import dao.TabelaEpisodioDAO;
import dao.TabelaSerieDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;


public class TelaPesquisaSerieGUI extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPesquisaTemporadaGUI frame = new TelaPesquisaTemporadaGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPesquisaSerieGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 586, 427);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {


				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();

				modelTable.setNumRows(0);

				TabelaSerieDAO daoTabelaSerie= new TabelaSerieDAO();

				ArrayList<ModeloTabelaSerie> serie = daoTabelaSerie.Listar();

				for(ModeloTabelaSerie a: serie){

					row.add(a.getProdutor());
					row.add(a.getNomeSerie());
					row.add(a.getElenco());
					row.add(a.getSinopse());
					row.add(a.getGenero());
					row.add(String.valueOf(a.getIndicacaoIdade()));
					row.add(a.getPremios());
					row.add(a.getAvaliacao());

					modelTable.addRow(row.toArray());
					row.clear();
				};	
			} 
		});
		btnPesquisar.setForeground(new Color(255, 105, 180));
		btnPesquisar.setBackground(new Color(230, 230, 250));
		btnPesquisar.setBounds(238, 342, 89, 23);
		contentPane.add(btnPesquisar);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(192, 32, 311, 33);
		contentPane.add(scrollPane);
		final JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setBackground(new Color(230, 230, 250));
		JButton btnPesquisarPor = new JButton("Pesquisar por...");
		btnPesquisarPor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();

				modelTable.setNumRows(0);

				TabelaSerieDAO daoTabelaSerie= new TabelaSerieDAO();

				ArrayList<ModeloTabelaSerie> TabelaSerie = daoTabelaSerie.Listar();

				String pesquisar = textArea.getText();

				for(ModeloTabelaSerie a : TabelaSerie){
					if(a.getNomeSerie().equals(pesquisar)){
						row.add(a.getProdutor());
						row.add(a.getNomeSerie());
						row.add(a.getElenco());
						row.add(a.getSinopse());
						row.add(a.getGenero());
						row.add(String.valueOf(a.getIndicacaoIdade()));
						row.add(a.getPremios());
						modelTable.addRow(row.toArray());
						row.clear();					
					}
				}
			}});
		btnPesquisarPor.setForeground(new Color(255, 105, 180));
		btnPesquisarPor.setBackground(new Color(230, 230, 250));
		btnPesquisarPor.setBounds(230, 89, 109, 23);
		contentPane.add(btnPesquisarPor);
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(26, 136, 477, 195);
		contentPane.add(scrollPane_1);
		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"Nome", "Produtor", "Elenco", "Sinopse", "G\u00EAnero", "Indica\u00E7\u00E3o ", "Pr\u00EAmios", "Avalia\u00E7\u00E3o"
				}
				));
		scrollPane_1.setViewportView(table);
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(10, 38, 46, 14);
		contentPane.add(lblNewLabel);
		
		JButton button = new JButton("Voltar");
		button.setForeground(new Color(255, 105, 180));
		button.setBounds(471, 354, 89, 23);
		contentPane.add(button);
	}
	/* comentario de teste*/
}
