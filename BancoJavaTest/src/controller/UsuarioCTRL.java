package controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import dao.TabelaSerieDAO;
import dao.TabelaUsuarioDAO;
import model.ModeloMySql;
import model.ModeloTabelaSerie;
import model.ModeloTabelaUsuario;
import view.CadastroSerieGUI;
import view.CadastroUsuarioGUI;

public class UsuarioCTRL {
	
	
	public void salvar(String nomeUsuario, String telefone, String email , String nacionalidade) {
		ModeloTabelaUsuario usuario = new ModeloTabelaUsuario(nomeUsuario, telefone, email ,nacionalidade);
		
		TabelaUsuarioDAO daoUsuario = new TabelaUsuarioDAO();
		//chama DAO
		if(daoUsuario.Inserir(usuario)){
			JOptionPane.showMessageDialog(null,"sucesso", "gravado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao gravar.",JOptionPane.ERROR_MESSAGE);
		}
		
	}

	public ArrayList<ModeloTabelaUsuario> getUsuario() {
		ArrayList<ModeloTabelaUsuario> usuario = null;
		TabelaUsuarioDAO daoUsuario = new TabelaUsuarioDAO();
		usuario = daoUsuario.Listar();
		return usuario;
	}

	public void deletarUsuario(String nomeUsuario) {
		int opcao = JOptionPane.CANCEL_OPTION; // Por padr�o assumimos como se o
		// usu�rio tivesse cancelado a
		// opera��o
		ArrayList<ModeloTabelaUsuario> usuario = getUsuario();
		if (usuario != null) {
			for (int i = 0; i < usuario.size(); i++) {
				String nomeusuario = usuario.get(i).getNomeUsuario();
				
				if (nomeusuario.equals(nomeUsuario) ) {
					opcao = JOptionPane.showConfirmDialog(null,
							"<html>Deseja deletar a serie:<br>cod.: " + nomeusuario + "<br>\"  ?</html>");
					break;
				}
			}
			if (opcao == JOptionPane.YES_OPTION) {
				TabelaUsuarioDAO daoUsuario = new TabelaUsuarioDAO();
				if(nomeUsuario != null && nomeUsuario.equals("")){
					//chama DAO
					daoUsuario.Delete("Nome usuario = '"+nomeUsuario+"'");
				}
				JOptionPane.showMessageDialog(null, "Usuario deletado");
			}
		}
	}

	public void configuraSenha(char[] password) {
		new String(password);
		JOptionPane.showMessageDialog(null, "Senha salva para esta sess�o");
	}
	
	



	
}
