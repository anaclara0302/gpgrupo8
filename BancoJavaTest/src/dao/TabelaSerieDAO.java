package dao;

import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;

import model.ModeloTabelaSerie;




public class TabelaSerieDAO {

	
	private ModeloMySql modelo = null;

	public TabelaSerieDAO(){

		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("db_projetoSeries");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}


	public boolean Inserir(ModeloTabelaSerie serie){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO serie (produtor, nomeSerie,elenco , sinopse, genero, indicacaoIdade, premios, avaliacao ) VALUES(?,?,?,?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			
			statement.setString (1, serie.getProdutor());
			statement.setString(2, serie.getNomeSerie());
			statement.setString(3, serie.getElenco());
			statement.setString(4, serie.getSinopse());
			statement.setString(5, serie.getGenero());
			statement.setInt(6, serie.getIndicacaoIdade());
			statement.setString(7, serie.getPremios());
			statement.setString(8, serie.getAvaliacao());
			
			

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	

	

	public boolean Alterar(ModeloTabelaSerie serie, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}

		/* prepara o sql para inserir no banco */

		String sql = " UPDATE serie SET produtor =?, nomeSerie = ?,elenco = ? , sinopse = ?, genero =?, indicacaoIdade =?, premios = ?, avaliacao =? WHERE nomeSerie=?;";



		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString (1, serie.getProdutor());
			statement.setString(2, serie.getNomeSerie());
			statement.setString(3, serie.getElenco());
			statement.setString(4, serie.getSinopse());
			statement.setString(5, serie.getGenero());
			statement.setInt(6, serie.getIndicacaoIdade());
			statement.setString(7, serie.getPremios());
			statement.setString(8, serie.getAvaliacao());
			statement.setString(9, key);
			
			System.out.println(statement);
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String nomeSerie) {
		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		String sql = "DELETE FROM serie where nomeSerie = ?;";
		

		try {
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1,nomeSerie);

			statement.executeUpdate();
			status = true;



		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return status;

	}public ArrayList<ModeloTabelaSerie> Listar(){
		
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaSerie> serie = new ArrayList<ModeloTabelaSerie>();
		
		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}
		
		
		/* prepara o sql para selecionar no banco */
		
		String sql = "SELECT * FROM serie;";
		
		/* prepara para selecionar no banco de dados */
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			/* recebe os resultados da query */
			while(result.next()){
				
			
				ModeloTabelaSerie series = new ModeloTabelaSerie();
				series.setProdutor(result.getString("produtor"));
				series.setNomeSerie(result.getString("nomeSerie"));
				series.setElenco(result.getString("elenco"));
				series.setSinopse(result.getString("sinopse"));
				series.setGenero(result.getString("genero"));
				series.setIndicacaoIdade(result.getInt("indicacaoIdade"));
				series.setPremios(result.getString("premios"));
				series.setAvaliacao(result.getString("avaliacao"));
				
				serie.add(series);
				
			}
						
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		
		}
		
		return serie;
	}
}



