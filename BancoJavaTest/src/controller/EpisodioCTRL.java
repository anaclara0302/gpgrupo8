package controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.ModeloTabelaEpisodio;
import view.CadastroEpisodioGUI;
import dao.TabelaEpisodioDAO;

public class EpisodioCTRL {
	
	public void salvar(String tempoTotalEp, String codigoEp) {
		ModeloTabelaEpisodio episodio = new ModeloTabelaEpisodio(tempoTotalEp, codigoEp);
		
		TabelaEpisodioDAO daoEpisodio = new TabelaEpisodioDAO();
		//chama DAO
		if(daoEpisodio.Inserir(episodio)){
			JOptionPane.showMessageDialog(null,"sucesso", "gravado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao gravar.",JOptionPane.ERROR_MESSAGE);
		}
		
	}

	public ArrayList<ModeloTabelaEpisodio> getEpisodio() {
		ArrayList<ModeloTabelaEpisodio> episodio = null;
		TabelaEpisodioDAO daoEpisodio = new TabelaEpisodioDAO();
		episodio = daoEpisodio.Listar();
		return episodio;
	}

	public void deletarEpisodio(String codigoEp) {
		int opcao = JOptionPane.CANCEL_OPTION; // Por padr�o assumimos como se o
		// usu�rio tivesse cancelado a
		// opera��o
		ArrayList<ModeloTabelaEpisodio> episodio = getEpisodio();
		if (episodio != null) {
			for (int i = 0; i < episodio.size(); i++) {
				String cod = episodio.get(i).getCodigoEp();
				
				if (cod.equals(codigoEp) ) {
					opcao = JOptionPane.showConfirmDialog(null,
							"<html>Deseja deletar o epis�dio:<br>cod.: " + cod + "<br>\"  ?</html>");
					break;
				}
			}
			if (opcao == JOptionPane.YES_OPTION) {
				TabelaEpisodioDAO daoEpisodio = new TabelaEpisodioDAO();
				if(codigoEp != null && codigoEp.equals("")){
					//chama DAO
					daoEpisodio.Delete(" Codigo = '"+codigoEp+"'");
				}
				JOptionPane.showMessageDialog(null, "Epis�dio deletado");
			}
		}
	}

	public void configuraSenha(char[] password) {
		new String(password);
		JOptionPane.showMessageDialog(null, "Senha salva para esta sess�o");
	}
	
	public TableModel getTableModel(String codigoEp) {
		ArrayList<ModeloTabelaEpisodio> episodio = getEpisodio();
		CadastroEpisodioGUI tela = new CadastroEpisodioGUI();
		if (episodio != null) {
			DefaultTableModel model = tela.modelTable();
			for (int i = 0; i < episodio.size(); i++) {
				String cod = episodio.get(i).getCodigoEp();
				String tempoTotalEp = episodio.get(i).getTempoTotalEp();
				
				
				if (cod.equals(codigoEp) ) {
					Object[] row ={tempoTotalEp,codigoEp}  ;
					model.addRow(row);
					break;
				}
			}
			
			
			
			
			return model;
		} else {
			JOptionPane.showMessageDialog(null, "N�o h� temporadas para listar");
			return null;
		}
	}


	public TableModel getTableModel() {
		ArrayList<ModeloTabelaEpisodio> episodio = getEpisodio();
		CadastroEpisodioGUI tela = new CadastroEpisodioGUI();
		if (episodio != null) {
			DefaultTableModel model = tela.modelTable();
			
			for (int i = 0; i < episodio.size(); i++) {
				String codigo = episodio.get(i).getCodigoEp();
				String tempoTotalEp = episodio.get(i).getTempoTotalEp();
				
				Object[] row ={tempoTotalEp, codigo }  ;
				model.addRow(row);
			}
			return model;
		} else {
			JOptionPane.showMessageDialog(null, "N�o h� temporadas para listar");
			return null;
		}
	}

}
