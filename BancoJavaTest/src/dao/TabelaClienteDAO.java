package dao;

import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.ModeloTabelaCliente;




public class TabelaClienteDAO {

	/***
	 * Criando o banco e a tabela.
	 * 
	 * CREATE DATABASE db_ifpocos;
	 * SHOW DATABASES;
	 * USE db_ifpocos;
	 * CREATE TABLE clientes (nome varchar(50),endereco varchar(50), idade int);
	 * SHOW TABLES;
	 * DESCRIBE clientes;
	 * SELECT * FROM clientes;
	 */

	private ModeloMySql modelo = null;

	public TabelaClienteDAO(){

		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("db_ifpocos");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}


	public boolean Inserir(ModeloTabelaCliente cliente){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO clientes (nome,endereco,idade) VALUES(?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, cliente.getNomeCliente());
			statement.setString(2, cliente.getEnderecoCliente());
			statement.setInt(3, cliente.getIdadeCliente());


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Alterar(ModeloTabelaCliente cliente, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}

		/* prepara o sql para inserir no banco */

		String sql = "UPDATE clientes SET nome=?, endereco=?, idade=? WHERE nome=?;";



		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, cliente.getNomeCliente());
			statement.setString(2, cliente.getEnderecoCliente());
			statement.setInt(3, cliente.getIdadeCliente());
			statement.setString(4, key);

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String nome) {
		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		String sql = "DELETE FROM clientes where nome = ?;";
		

		try {
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1,nome);

			statement.executeUpdate();
			status = true;



		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return status;

	}public ArrayList<ModeloTabelaCliente> Listar(){
		
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaCliente> clientes = new ArrayList<ModeloTabelaCliente>();
		
		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}
		
		
		/* prepara o sql para selecionar no banco */
		
		String sql = "SELECT * FROM clientes;";
		
		/* prepara para selecionar no banco de dados */
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			/* recebe os resultados da query */
			while(result.next()){
				ModeloTabelaCliente cliente = new ModeloTabelaCliente();
				cliente.setEnderecoCliente(result.getString("Endereco"));
				cliente.setIdadeCliente(result.getInt("idade"));
				cliente.setNomeCliente(result.getString("Nome"));
				clientes.add(cliente);
				
			}
						
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		
		}
		
		return clientes;
	}
}


