package view;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;


public class TelaPesquisaTemporadaGUI extends JFrame {

private JPanel contentPane;
private JTable table;

/**
* Launch the application.
*/
public static void main(String[] args) {
EventQueue.invokeLater(new Runnable() {
public void run() {
try {
TelaPesquisaTemporadaGUI frame = new TelaPesquisaTemporadaGUI();
frame.setVisible(true);
} catch (Exception e) {
e.printStackTrace();
}
}
});
}

/**
* Create the frame.
*/
public TelaPesquisaTemporadaGUI() {
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setBounds(100, 100, 495, 384);
contentPane = new JPanel();
contentPane.setBackground(new Color(0, 0, 0));
contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
setContentPane(contentPane);
contentPane.setLayout(null);
JScrollPane scrollPane = new JScrollPane();
scrollPane.setBounds(102, 39, 335, 30);
contentPane.add(scrollPane);
JTextArea textArea = new JTextArea();
textArea.setBackground(new Color(255, 255, 255));
scrollPane.setViewportView(textArea);
JButton btnPesquisarPor = new JButton("Pesquisar por...");
btnPesquisarPor.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
	}
});
btnPesquisarPor.setForeground(new Color(70,130,180));
btnPesquisarPor.setBounds(214, 80, 109, 23);
contentPane.add(btnPesquisarPor);
JScrollPane scrollPane_1 = new JScrollPane();
scrollPane_1.setBounds(102, 120, 335, 167);
contentPane.add(scrollPane_1);
table = new JTable();
table.setModel(new DefaultTableModel(
new Object[][] {
},
new String[] {
"C\u00F3digo", "Tempo Total"
}
));
scrollPane_1.setViewportView(table);
JButton btnPesquisar = new JButton("Pesquisar");
btnPesquisar.setForeground(new Color(70,130,180));
btnPesquisar.setBackground(new Color(230, 230, 250));
btnPesquisar.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
	}
});
btnPesquisar.setBounds(214, 298, 109, 23);
contentPane.add(btnPesquisar);
JLabel lblPesquisar = new JLabel("Pesquisar:");
lblPesquisar.setForeground(new Color(255, 69, 0));
lblPesquisar.setBounds(10, 40, 70, 24);
contentPane.add(lblPesquisar);
JButton button = new JButton("Voltar");
button.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		dispose();
	}
});
button.setForeground(new Color(70,130,180));
button.setBounds(348, 298, 89, 23);
contentPane.add(button);
}

}