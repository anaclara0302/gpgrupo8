package model;

public class ModeloTabelaSerie {
	private String produtor;
	private String nomeSerie;
	private String elenco;
	private String sinopse;
	private String genero;
	private int indicacaoIdade;
	private String premios;
	private String avaliacao;
	
	
	public ModeloTabelaSerie(String produtor, String nomeSerie, String elenco,
			String sinopse, String genero, int indicacaoIdade, String premios
			) {
		super();
		this.produtor = produtor;
		this.nomeSerie = nomeSerie;
		this.elenco = elenco;
		this.sinopse = sinopse;
		this.genero = genero;
		this.indicacaoIdade = indicacaoIdade;
		this.premios = premios;
		
	}


	public ModeloTabelaSerie() {
		super();
	}


	public String getProdutor() {
		return produtor;
	}


	public void setProdutor(String produtor) {
		this.produtor = produtor;
	}


	public String getNomeSerie() {
		return nomeSerie;
	}


	public void setNomeSerie(String nomeSerie) {
		this.nomeSerie = nomeSerie;
	}


	public String getElenco() {
		return elenco;
	}


	public void setElenco(String elenco) {
		this.elenco = elenco;
	}


	public String getSinopse() {
		return sinopse;
	}


	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}


	public int getIndicacaoIdade() {
		return indicacaoIdade;
	}


	public void setIndicacaoIdade(int indicacaoIdade) {
		this.indicacaoIdade = indicacaoIdade;
	}


	public String getPremios() {
		return premios;
	}


	public void setPremios(String premios) {
		this.premios = premios;
	}


	public String getAvaliacao() {
		return avaliacao;
	}


	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	
	
	

}
