package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JLabel;

import model.ModeloTabelaCompra;
import model.ModeloTabelaEpisodio;
import model.ModeloTabelaSerie;
import dao.CompraDAO;
import dao.TabelaEpisodioDAO;
import dao.TabelaSerieDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.Color;

public class TelaPesquisaEpisodioGUI extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPesquisaEpisodioGUI frame = new TelaPesquisaEpisodioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPesquisaEpisodioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 471, 321);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 69, 0));
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(93, 37, 341, 32);
		contentPane.add(scrollPane);

		final JTextArea textArea = new JTextArea();
		scrollPane.setRowHeaderView(textArea);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(new EmptyBorder(6, 0, 4, 0));
		scrollPane_1.setEnabled(false);
		scrollPane_1.setBounds(93, 114, 341, 112);
		contentPane.add(scrollPane_1);

		table = new JTable();
		table.setBackground(Color.GREEN);
		table.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"C\u00F3digo", "Tempo Total"
				}
				));
		scrollPane_1.setViewportView(table);



		JButton btnPesquisarPor = new JButton("Pesquisar Por");
		btnPesquisarPor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();

				modelTable.setNumRows(0);

				TabelaEpisodioDAO daoTabelaEpisodio= new TabelaEpisodioDAO();

				ArrayList<ModeloTabelaEpisodio> TabelaEpisodio = daoTabelaEpisodio.Listar();

				String pesquisarPor = textArea.getText();

				for(ModeloTabelaEpisodio a : TabelaEpisodio){
					if(a.getCodigoEp().equals(pesquisarPor)){
						row.add(a.getTempoTotalEp());
						row.add(a.getCodigoEp());
						modelTable.addRow(row.toArray());
						row.clear();					
					}
				}}});


		btnPesquisarPor.setBounds(345, 80, 89, 23);
		contentPane.add(btnPesquisarPor);

		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {


				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();

				modelTable.setNumRows(0);

				TabelaEpisodioDAO daoTabelaEpisodio= new TabelaEpisodioDAO();

				ArrayList<ModeloTabelaEpisodio> episodio = daoTabelaEpisodio.Listar();

				for(ModeloTabelaEpisodio a: episodio){


					row.add(a.getTempoTotalEp());
					row.add(a.getCodigoEp());



					modelTable.addRow(row.toArray());
					row.clear();
				}
			};
		});
		btnPesquisar.setBounds(345, 237, 89, 23);
		contentPane.add(btnPesquisar);

		JLabel lblNewLabel = new JLabel("Pesquisar");
		lblNewLabel.setForeground(new Color(255, 69, 0));
		lblNewLabel.setBackground(new Color(0, 0, 0));
		lblNewLabel.setBounds(20, 43, 46, 14);
		contentPane.add(lblNewLabel);
	}
}
