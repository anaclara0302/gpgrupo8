package view;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JScrollPane;

import model.ModeloTabelaCliente;
import model.ModeloTabelaEpisodio;
import dao.TabelaClienteDAO;
import dao.TabelaEpisodioDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class CadastroEpisodioGUI extends JFrame {

private JPanel contentPane;
private JTextField txtCod;
private JTextField txtTempoT;
private JTextField txtCodAntigo;

/**
* Launch the application.
*/
public static void main(String[] args) {
EventQueue.invokeLater(new Runnable() {
public void run() {
try {
CadastroEpisodioGUI frame = new CadastroEpisodioGUI();
frame.setVisible(true);
} catch (Exception e) {
e.printStackTrace();
}
}
});
}

/**
* Create the frame.
*/
public CadastroEpisodioGUI() {
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setBounds(100, 100, 455, 252);
contentPane = new JPanel();
contentPane.setBackground(new Color(224, 255, 255));
contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
setContentPane(contentPane);
contentPane.setLayout(null);
txtCod = new JTextField();
txtCod.setBackground(new Color(230, 230, 250));
txtCod.setBounds(85, 52, 118, 20);
contentPane.add(txtCod);
txtCod.setColumns(10);
JLabel lblNome = new JLabel("C\u00F3digo:");
lblNome.setForeground(new Color(255, 105, 180));
lblNome.setBounds(10, 55, 46, 14);
contentPane.add(lblNome);
JLabel lblProdutor = new JLabel("Tempo Total:");
lblProdutor.setForeground(new Color(255, 105, 180));
lblProdutor.setBounds(10, 151, 95, 14);
contentPane.add(lblProdutor);
txtTempoT = new JTextField();
txtTempoT.setBackground(new Color(230, 230, 250));
txtTempoT.setBounds(85, 148, 118, 20);
contentPane.add(txtTempoT);
txtTempoT.setColumns(10);
JButton btnInserir = new JButton("Cadastrar");
btnInserir.setBackground(new Color(230, 230, 250));
btnInserir.setForeground(new Color(255, 105, 180));
btnInserir.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent arg0) {
		
		/* recupera os dados da interface gráfica */
		String codigoEp = txtCod.getText();
		String tempoTotalEp = txtTempoT.getText();


		/* cria um objeto para encapsular os campos */
		ModeloTabelaEpisodio episodio = new ModeloTabelaEpisodio(tempoTotalEp,codigoEp);


		/* cria o objeto de dao */
		TabelaEpisodioDAO daoEpisodio = new TabelaEpisodioDAO();

		if(daoEpisodio.Inserir(episodio)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}
	}
	
});
btnInserir.setBounds(317, 79, 89, 23);
contentPane.add(btnInserir);
JButton btnAlterar = new JButton("Alterar");
btnAlterar.setBackground(new Color(230, 230, 250));
btnAlterar.setForeground(new Color(255, 105, 180));
btnAlterar.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		
		/* recupera os dados da interface gráfica */
		String tempoTotalEp = txtTempoT.getText();
		String codigoEp = txtCod.getText();
		String codigoAntigo = txtCodAntigo.getText();

		/* cria um objeto para encapsular os campos */
		ModeloTabelaEpisodio episodio = new ModeloTabelaEpisodio(tempoTotalEp,codigoEp);


		/* cria o objeto de dao */
		TabelaEpisodioDAO daoEpisodio = new TabelaEpisodioDAO();
		
		
		if(daoEpisodio.Alterar(episodio, codigoAntigo)){
			JOptionPane.showMessageDialog(null, "alterado com sucesso!!");
		}
		else{
			JOptionPane.showMessageDialog(null, "n�o foi posss�vel alterar!!");
			
		}
			
		
	}
});
btnAlterar.setBounds(317, 147, 89, 23);
contentPane.add(btnAlterar);
JButton btnDeletar = new JButton("Deletar");
btnDeletar.setBackground(new Color(230, 230, 250));
btnDeletar.setForeground(new Color(255, 105, 180));
btnDeletar.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent arg0) {
		
		TabelaEpisodioDAO daoEpisodio = new TabelaEpisodioDAO();
		String tempoTotalEp = txtTempoT.getText();
		String codigoEp = txtCod.getText();
		
		if(daoEpisodio.Delete(codigoEp)){
			JOptionPane.showMessageDialog(null, "deletado com sucesso!!");
		}
		else{
			JOptionPane.showMessageDialog(null, "n�o foi posss�vel deletar!!");
			
		}
		
	}
});
btnDeletar.setBounds(317, 113, 89, 23);
contentPane.add(btnDeletar);
txtCodAntigo = new JTextField();
txtCodAntigo.setBackground(new Color(230, 230, 250));
txtCodAntigo.setColumns(10);
txtCodAntigo.setBounds(85, 97, 118, 20);
contentPane.add(txtCodAntigo);
JLabel lblCdigoAntigo = new JLabel("C\u00F3digo Antigo:");
lblCdigoAntigo.setForeground(new Color(255, 105, 180));
lblCdigoAntigo.setBounds(10, 100, 79, 14);
contentPane.add(lblCdigoAntigo);
JTextArea textArea_1 = new JTextArea();
textArea_1.setForeground(Color.WHITE);
textArea_1.setBackground(new Color(221, 160, 221));
textArea_1.setBounds(141, 272, 340, 28);
}

public DefaultTableModel modelTable() {
	// TODO Auto-generated method stub
	return null;
}
}