package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JScrollPane;

import model.ModeloTabelaCliente;
import model.ModeloTabelaSerie;
import dao.CompraDAO;
import dao.TabelaClienteDAO;
import dao.TabelaSerieDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;


public class CadastroSerieGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtProdutor;
	private JTextField txtElenco;
	private JTextField txtSinopse;
	private JTextField txtIndicacaoIdade;
	private JTextField txtPremios;
	private JTextField txtGenero;
	private JTextField txtNomeAntigo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroSerieGUI frame = new CadastroSerieGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroSerieGUI() {
		setForeground(new Color(255, 105, 180));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 433);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 105, 180));
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		txtNome = new JTextField();
		txtNome.setBackground(new Color(230, 230, 250));
		txtNome.setBounds(66, 20, 362, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setForeground(new Color(255, 105, 180));
		lblNome.setBackground(new Color(75, 0, 130));
		lblNome.setBounds(10, 23, 46, 14);
		contentPane.add(lblNome);
		JLabel lblProdutor = new JLabel("Produtor:");
		lblProdutor.setForeground(new Color(255, 105, 180));
		lblProdutor.setBackground(new Color(75, 0, 130));
		lblProdutor.setBounds(10, 54, 46, 14);
		contentPane.add(lblProdutor);
		txtProdutor = new JTextField();
		txtProdutor.setBackground(new Color(230, 230, 250));
		txtProdutor.setBounds(66, 51, 149, 20);
		contentPane.add(txtProdutor);
		txtProdutor.setColumns(10);
		JLabel lblElenco = new JLabel("Elenco:");
		lblElenco.setForeground(new Color(255, 105, 180));
		lblElenco.setBackground(new Color(75, 0, 130));
		lblElenco.setBounds(225, 51, 46, 14);
		contentPane.add(lblElenco);
		txtElenco = new JTextField();
		txtElenco.setBackground(new Color(230, 230, 250));
		txtElenco.setBounds(277, 51, 149, 113);
		contentPane.add(txtElenco);
		txtElenco.setColumns(10);
		JLabel lblSinopse = new JLabel("Sinopse:");
		lblSinopse.setForeground(new Color(255, 105, 180));
		lblSinopse.setBackground(new Color(75, 0, 130));
		lblSinopse.setBounds(10, 191, 46, 14);
		contentPane.add(lblSinopse);
		txtSinopse = new JTextField();
		txtSinopse.setBackground(new Color(230, 230, 250));
		txtSinopse.setBounds(66, 191, 369, 137);
		contentPane.add(txtSinopse);
		txtSinopse.setColumns(10);
		JLabel lblIndicao = new JLabel("Indica\u00E7\u00E3o:");
		lblIndicao.setForeground(new Color(255, 105, 180));
		lblIndicao.setBackground(new Color(75, 0, 130));
		lblIndicao.setBounds(10, 117, 69, 14);
		contentPane.add(lblIndicao);
		txtIndicacaoIdade = new JTextField();
		txtIndicacaoIdade.setBackground(new Color(230, 230, 250));
		txtIndicacaoIdade.setBounds(66, 114, 149, 20);
		contentPane.add(txtIndicacaoIdade);
		txtIndicacaoIdade.setColumns(10);
		JLabel lblPrmios = new JLabel("Pr\u00EAmios:");
		lblPrmios.setForeground(new Color(255, 105, 180));
		lblPrmios.setBackground(new Color(75, 0, 130));
		lblPrmios.setBounds(10, 144, 46, 14);
		contentPane.add(lblPrmios);
		txtPremios = new JTextField();
		txtPremios.setBackground(new Color(230, 230, 250));
		txtPremios.setBounds(66, 141, 149, 20);
		contentPane.add(txtPremios);
		txtPremios.setColumns(10);
		JButton btnInserir = new JButton("Cadastrar");
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {


				/* recupera os dados da interface gráfica */

				String produtor = txtProdutor.getText();
				int indicacaoIdade = Integer.parseInt(txtIndicacaoIdade.getText());
				String nome = txtNome.getText();
				String elenco = txtElenco.getText();
				String premios = txtPremios.getText();
				String sinopse = txtSinopse.getText();
				String genero = txtGenero.getText();



				/* cria um objeto para encapsular os campos */
				ModeloTabelaSerie serie = new ModeloTabelaSerie(produtor,nome,elenco,sinopse,genero,indicacaoIdade,premios);


				/* cria o objeto de dao */
				TabelaSerieDAO daoSerie = new TabelaSerieDAO();

				if(daoSerie.Inserir(serie)){

					JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
				} else {
					JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
				}

			}


		});
		btnInserir.setForeground(new Color(255, 105, 180));
		btnInserir.setBounds(477, 248, 89, 23);
		contentPane.add(btnInserir);
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {



				/* recupera os dados da interface gráfica */
				String produtor = txtProdutor.getText();
				int indicacaoIdade = Integer.parseInt(txtIndicacaoIdade.getText());
				String nome = txtNome.getText();
				String elenco = txtElenco.getText();
				String premios = txtPremios.getText();
				String sinopse = txtSinopse.getText();
				String genero = txtGenero.getText();
				String nomeAntigo = txtNomeAntigo.getText();


				/* cria um objeto para encapsular os campos */
				ModeloTabelaSerie serie = new ModeloTabelaSerie(produtor,nome,elenco,sinopse,genero,indicacaoIdade,premios);


				/* cria o objeto de ctrl */
				TabelaSerieDAO daoSerie = new TabelaSerieDAO();


				if(daoSerie.Alterar(serie, nomeAntigo)){
					JOptionPane.showMessageDialog(null, "alterado com sucesso!!");
				}
				else{
					JOptionPane.showMessageDialog(null, "n�o foi posss�vel alterar!!");

				}
			}
		});
		btnAlterar.setForeground(new Color(255, 105, 180));
		btnAlterar.setBounds(477, 316, 89, 23);
		contentPane.add(btnAlterar);
		JButton btnDeletar = new JButton("Deletar");
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				TabelaSerieDAO daoSerie = new TabelaSerieDAO();
				String nome = txtNome.getText();
				
				if(daoSerie.Delete(nome)){
					JOptionPane.showMessageDialog(null, "deletado com sucesso!!");
				}
				else{
					JOptionPane.showMessageDialog(null, "n�o foi posss�vel deletar!!");
					
				}
				//hhhhh
			}
		});
		btnDeletar.setForeground(new Color(255, 105, 180));
		btnDeletar.setBounds(477, 282, 89, 23);
		contentPane.add(btnDeletar);

		txtGenero = new JTextField();
		txtGenero.setColumns(10);
		txtGenero.setBackground(new Color(230, 230, 250));
		txtGenero.setBounds(66, 82, 149, 20);
		contentPane.add(txtGenero);

		JLabel lblGnero = new JLabel("G\u00EAnero:");
		lblGnero.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 12));
		lblGnero.setForeground(new Color(255, 105, 180));
		lblGnero.setBackground(new Color(75, 0, 130));
		lblGnero.setBounds(10, 86, 46, 14);
		contentPane.add(lblGnero);

		JLabel lblNomeAntigo = new JLabel("Nome antigo:");
		lblNomeAntigo.setForeground(new Color(255, 105, 180));
		lblNomeAntigo.setBounds(10, 370, 69, 14);
		contentPane.add(lblNomeAntigo);

		txtNomeAntigo = new JTextField();
		txtNomeAntigo.setBackground(new Color(230, 230, 250));
		txtNomeAntigo.setBounds(84, 364, 351, 20);
		contentPane.add(txtNomeAntigo);
		txtNomeAntigo.setColumns(10);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setForeground(new Color(255, 105, 180));
		btnVoltar.setBounds(477, 350, 89, 23);
		contentPane.add(btnVoltar);
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setForeground(Color.WHITE);
		textArea_1.setBackground(new Color(221, 160, 221));
		textArea_1.setBounds(141, 272, 340, 28);
	}

	public DefaultTableModel modelTable() {
		// TODO Auto-generated method stub
		return null;
	}
}
