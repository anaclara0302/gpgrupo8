package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JScrollPane;

import model.ModeloTabelaEpisodio;
import model.ModeloTabelaTemporada;
import dao.TabelaEpisodioDAO;
import dao.TabelaTemporadaDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;


public class CadastroTemporadaGUI extends JFrame {

private JPanel contentPane;
private JTextField txtCod;
private JTextField txtTempoTemp;
private JTextField txtcodigoAntigo;

/**
* Launch the application.
*/
public static void main(String[] args) {
EventQueue.invokeLater(new Runnable() {
public void run() {
try {
CadastroTemporadaGUI frame = new CadastroTemporadaGUI();
frame.setVisible(true);
} catch (Exception e) {
e.printStackTrace();
}
}
});
}

/**
* Create the frame.
*/
public CadastroTemporadaGUI() {
setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
setBounds(100, 100, 455, 252);
contentPane = new JPanel();
contentPane.setBackground(new Color(224, 255, 255));
contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
setContentPane(contentPane);
contentPane.setLayout(null);
txtCod = new JTextField();
txtCod.setBackground(new Color(230, 230, 250));
txtCod.setBounds(100, 45, 118, 20);
contentPane.add(txtCod);
txtCod.setColumns(10);
JLabel lblNome = new JLabel("C\u00F3digo:");
lblNome.setFont(new Font("Tahoma", Font.PLAIN, 11));
lblNome.setForeground(new Color(255, 105, 180));
lblNome.setBounds(10, 47, 46, 14);
contentPane.add(lblNome);
JLabel lblProdutor = new JLabel("Tempo Total:");
lblProdutor.setFont(new Font("Tahoma", Font.PLAIN, 11));
lblProdutor.setForeground(new Color(255, 105, 180));
lblProdutor.setBounds(10, 102, 95, 14);
contentPane.add(lblProdutor);
txtTempoTemp = new JTextField();
txtTempoTemp.setBackground(new Color(230, 230, 250));
txtTempoTemp.setBounds(100, 100, 118, 20);
contentPane.add(txtTempoTemp);
txtTempoTemp.setColumns(10);
JButton btnInserir = new JButton("Cadastrar");
btnInserir.setFont(new Font("Tahoma", Font.PLAIN, 11));
btnInserir.setBackground(new Color(230, 230, 250));
btnInserir.setForeground(new Color(255, 105, 180));
btnInserir.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent arg0) {
		
				
				/* recupera os dados da interface gráfica */
				String codigo = txtCod.getText();
				String tempoTemporada = txtTempoTemp.getText();


				/* cria um objeto para encapsular os campos */
				ModeloTabelaTemporada temporada = new ModeloTabelaTemporada(tempoTemporada,codigo);


				/* cria o objeto de dao */
				TabelaTemporadaDAO daoTemporada = new TabelaTemporadaDAO();

				if(daoTemporada.Inserir(temporada)){

					JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
				} else {
					JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
				}
			}
				
	
});
btnInserir.setBounds(297, 79, 109, 23);
contentPane.add(btnInserir);
JButton btnAlterar = new JButton("Alterar");
btnAlterar.setFont(new Font("Tahoma", Font.PLAIN, 11));
btnAlterar.setBackground(new Color(230, 230, 250));
btnAlterar.setForeground(new Color(255, 105, 180));
btnAlterar.addActionListener(new ActionListener() {
	
	
	public void actionPerformed(ActionEvent e) {
		/* recupera os dados da interface gráfica */
		String tempoTemporada = txtTempoTemp.getText();
		String codigoEp = txtCod.getText();
		String codigoAntigo = txtcodigoAntigo.getText();

		/* cria um objeto para encapsular os campos */
		ModeloTabelaTemporada temporada = new ModeloTabelaTemporada(tempoTemporada,codigoEp);


		/* cria o objeto de dao */
		TabelaTemporadaDAO daoTemporada = new TabelaTemporadaDAO();
		
		
		if(daoTemporada.Alterar(temporada, codigoAntigo)){
			JOptionPane.showMessageDialog(null, "alterado com sucesso!!");
		}
		else{
			JOptionPane.showMessageDialog(null, "n�o foi posss�vel alterar!!");
			
		}
				
		
	}
});
btnAlterar.setBounds(297, 145, 109, 23);
contentPane.add(btnAlterar);
JButton btnDeletar = new JButton("Deletar");
btnDeletar.setFont(new Font("Tahoma", Font.PLAIN, 11));
btnDeletar.setBackground(new Color(230, 230, 250));
btnDeletar.setForeground(new Color(255, 105, 180));
btnDeletar.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
		
		TabelaTemporadaDAO daoTemporada = new TabelaTemporadaDAO();
		String tempoTemporada = txtTempoTemp.getText();
		String codigoEp = txtCod.getText();
		
		if(daoTemporada.Delete(codigoEp)){
			JOptionPane.showMessageDialog(null, "deletado com sucesso!!");
		}
		else{
			JOptionPane.showMessageDialog(null, "n�o foi posss�vel deletar!!");
			
		}
		
	}
});
btnDeletar.setBounds(297, 113, 109, 23);
contentPane.add(btnDeletar);
JLabel lblNewLabel = new JLabel("C\u00F3digo Antigo:");
lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
lblNewLabel.setForeground(new Color(255, 105, 180));
lblNewLabel.setBounds(10, 151, 84, 14);
contentPane.add(lblNewLabel);
txtcodigoAntigo = new JTextField();
txtcodigoAntigo.setBackground(new Color(230, 230, 250));
txtcodigoAntigo.setBounds(100, 148, 118, 20);
contentPane.add(txtcodigoAntigo);
txtcodigoAntigo.setColumns(10);
JTextArea textArea_1 = new JTextArea();
textArea_1.setForeground(Color.WHITE);
textArea_1.setBackground(new Color(221, 160, 221));
textArea_1.setBounds(141, 272, 340, 28);
}

public DefaultTableModel modelTable() {
	// TODO Auto-generated method stub
	return null;
}
}
