package model;

public class ModeloTabelaTemporada {
	private String tempoTemporada;
	private String codigo;
	
	
	public ModeloTabelaTemporada(String tempoTemporada, String codigo) {
		super();
		this.tempoTemporada = tempoTemporada;
		this.codigo = codigo;
	}


	public String getTempoTemporada() {
		return tempoTemporada;
	}


	public void setTempoTemporada(String tempoTemporada) {
		this.tempoTemporada = tempoTemporada;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public ModeloTabelaTemporada() {
		super();
	}
	
}
