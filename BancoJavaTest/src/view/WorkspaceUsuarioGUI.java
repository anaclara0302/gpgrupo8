package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.JMenuBar;

import java.awt.Panel;
import java.awt.Button;

import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Scrollbar;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Font;

import javax.swing.JList;


public class WorkspaceUsuarioGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WorkspaceUsuarioGUI frame = new WorkspaceUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public WorkspaceUsuarioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 827, 533);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(395, 26, 163, 23);
		contentPane.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(new Color(220, 220, 220));
		scrollPane.setViewportView(textArea);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(258, 110, 322, 322);
		contentPane.add(scrollPane_1);
		
		Scrollbar scrollbar = new Scrollbar();
		scrollbar.setBackground(new Color(220, 220, 220));
		scrollPane_1.setRowHeaderView(scrollbar);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(26, 128, 150, 307);
		contentPane.add(scrollPane_2);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBackground(new Color(211, 211, 211));
		scrollPane_2.setViewportView(editorPane);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\14151000030\\Downloads\\bloggif_5845a1f389fe3.png"));
		lblNewLabel.setBounds(568, 26, 25, 25);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(667, 110, 96, 322);
		contentPane.add(scrollPane_3);
		
		JList list = new JList();
		list.setBackground(new Color(220, 220, 220));
		scrollPane_3.setViewportView(list);
		
		JLabel lblNewLabel_1 = new JLabel("SERIESONIT");
		lblNewLabel_1.setForeground(new Color(255, 99, 71));
		lblNewLabel_1.setFont(new Font("Candara", Font.BOLD, 20));
		lblNewLabel_1.setBounds(26, 0, 234, 38);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				JOptionPane.showInputDialog(null, "Digite serie que deseja calcular");
			}
		});
		lblNewLabel_2.setIcon(new ImageIcon("\\\\fs05.pcs.ifsuldeminas.local\\Usuarios\\Alunos\\Regulares\\2015\\14151000030\\Desktop\\workspace Certa\\relogio.png"));
		lblNewLabel_2.setBounds(26, 47, 70, 70);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setForeground(new Color(230, 230, 250));
		lblNewLabel_3.setBackground(new Color(245, 245, 245));
		lblNewLabel_3.setBounds(697, 26, 50, 50);
		contentPane.add(lblNewLabel_3);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setEditable(false);
		textArea_1.setBounds(134, 47, 102, 20);
		contentPane.add(textArea_1);
		
		JTextArea textArea_2 = new JTextArea();
		textArea_2.setEditable(false);
		textArea_2.setBounds(134, 70, 102, 20);
		contentPane.add(textArea_2);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
