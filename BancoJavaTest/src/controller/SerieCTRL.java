package controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import dao.TabelaSerieDAO;
import model.ModeloMySql;
import model.ModeloTabelaSerie;
import view.CadastroSerieGUI;

public class SerieCTRL {
	
	
	public void salvar(String produtor, String nomeSerie, String elenco , String sinopse, String genero, int indicacaoIdade, String premios) {
		ModeloTabelaSerie serie = new ModeloTabelaSerie(produtor, nomeSerie,elenco , sinopse, genero, indicacaoIdade, premios);
		
		TabelaSerieDAO daoSerie = new TabelaSerieDAO();
		//chama DAO
		if(daoSerie.Inserir(serie)){
			JOptionPane.showMessageDialog(null,"sucesso", "gravado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao gravar.",JOptionPane.ERROR_MESSAGE);
		}
		
	}

	public ArrayList<ModeloTabelaSerie> getSerie() {
		ArrayList<ModeloTabelaSerie> serie = null;
		TabelaSerieDAO daoSerie = new TabelaSerieDAO();
		serie = daoSerie.Listar();
		return serie;
	}

	public void deletarSerie(String nomeSerie) {
		int opcao = JOptionPane.CANCEL_OPTION; // Por padr�o assumimos como se o
		// usu�rio tivesse cancelado a
		// opera��o
		ArrayList<ModeloTabelaSerie> serie = getSerie();
		if (serie != null) {
			for (int i = 0; i < serie.size(); i++) {
				String nomeserie = serie.get(i).getNomeSerie();
				
				if (nomeserie.equals(nomeSerie) ) {
					opcao = JOptionPane.showConfirmDialog(null,
							"<html>Deseja deletar a serie:<br>cod.: " + nomeserie + "<br>\"  ?</html>");
					break;
				}
			}
			if (opcao == JOptionPane.YES_OPTION) {
				TabelaSerieDAO daoSerie = new TabelaSerieDAO();
				if(nomeSerie != null && nomeSerie.equals("")){
					//chama DAO
					daoSerie.Delete("Nome Serie = '"+nomeSerie+"'");
				}
				JOptionPane.showMessageDialog(null, "Serie deletada");
			}
		}
	}

	public void configuraSenha(char[] password) {
		new String(password);
		JOptionPane.showMessageDialog(null, "Senha salva para esta sess�o");
	}
	
	public TableModel getTableModel(String nomeSerie) {
		ArrayList<ModeloTabelaSerie> serie = getSerie();
		CadastroSerieGUI tela = new CadastroSerieGUI();
		if (serie != null) {
			DefaultTableModel model = tela.modelTable();
			for (int i = 0; i < serie.size(); i++) {
				String produtor = serie.get(i).getProdutor();
				String nomeserie = serie.get(i).getNomeSerie();
				String elenco = serie.get(i).getElenco();
				String sinopse = serie.get(i).getSinopse();
				String genero = serie.get(i).getGenero();
				int indicacaoIdade = serie.get(i).getIndicacaoIdade();
				String premios = serie.get(i).getPremios();
				
				
				if (nomeserie.equals(nomeSerie) ) {
					Object[] row ={ produtor , nomeSerie ,elenco, sinopse , genero , indicacaoIdade , premios}  ;
					model.addRow(row);
					break;
				}
			}
			
			
			
			
			return model;
		} else {
			JOptionPane.showMessageDialog(null, "N�o h� series para listar");
			return null;
		}
	}


	public TableModel getTableModel() {
		ArrayList<ModeloTabelaSerie> serie = getSerie();
		CadastroSerieGUI tela = new CadastroSerieGUI();
		if (serie != null) {
			DefaultTableModel model = tela.modelTable();
			
			for (int i = 0; i < serie.size(); i++) {
				String produtor = serie.get(i).getProdutor();
				String nomeserie = serie.get(i).getNomeSerie();
				String elenco = serie.get(i).getElenco();
				String sinopse = serie.get(i).getSinopse();
				String genero = serie.get(i).getGenero();
				int indicacaoIdade = serie.get(i).getIndicacaoIdade();
				String premios = serie.get(i).getPremios();
				
				Object[] row ={ produtor , nomeserie ,elenco, sinopse , genero , indicacaoIdade , premios}  ;
				model.addRow(row);
			}
			return model;
		} else {
			JOptionPane.showMessageDialog(null, "N�o h� series para listar");
			return null;
		}
	}

	
}
