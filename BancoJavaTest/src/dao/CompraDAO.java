package dao;

import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;

import model.ModeloTabelaCompra;




public class CompraDAO {

	
	private ModeloMySql modelo = null;

	public CompraDAO(){

		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("db_ifpocos");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}


	public boolean Inserir(ModeloTabelaCompra compra){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO compra (codigo,descricao, fornecedor, tipo, valor) VALUES(?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, compra.getCodigo());
			statement.setString(2, compra.getDescricao());
			statement.setString(3, compra.getFornecedor());
			statement.setString(4, compra.getTipo());
			statement.setDouble(5, compra.getValor());


			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	

	

	public boolean Alterar(ModeloTabelaCompra compra, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}

		/* prepara o sql para inserir no banco */

		String sql = " UPDATE compra SET codigo=?, descricao =?, fornecedor=?, tipo=?, valor=? WHERE codigo=?;";



		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, compra.getCodigo());
			statement.setString(2, compra.getDescricao());
			statement.setString(3, compra.getFornecedor());
			statement.setString(4, compra.getTipo());
			statement.setDouble(5, compra.getValor());
			statement.setString(6, key);
			System.out.println(statement);
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String codigo) {
		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		String sql = "DELETE FROM compra where codigo = ?;";
		

		try {
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1,codigo);

			statement.executeUpdate();
			status = true;



		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return status;

	}public ArrayList<ModeloTabelaCompra> Listar(){
		
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaCompra> compra = new ArrayList<ModeloTabelaCompra>();
		
		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}
		
		
		/* prepara o sql para selecionar no banco */
		
		String sql = "SELECT * FROM compra;";
		
		/* prepara para selecionar no banco de dados */
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			/* recebe os resultados da query */
			while(result.next()){
				ModeloTabelaCompra compras = new ModeloTabelaCompra();
				compras.setCodigo(result.getString("codigo"));
				compras.setDescricao(result.getString("descricao"));
				compras.setFornecedor(result.getString("fornecedor"));
				compras.setTipo(result.getString("Tipo"));
				compras.setValor(result.getDouble("Valor"));
			
				compra.add(compras);
				
			}
						
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		
		}
		
		return compra;
	}
}



