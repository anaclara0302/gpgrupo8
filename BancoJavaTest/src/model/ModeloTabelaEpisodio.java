package model;

public class ModeloTabelaEpisodio {
	private String tempoTotalEp;
	private String codigoEp;
	
	public ModeloTabelaEpisodio() {
		super();
	}
	public ModeloTabelaEpisodio(String tempoTotalEp, String codigoEp) {
		super();
		this.tempoTotalEp = tempoTotalEp;
		this.codigoEp = codigoEp;
	}
	public String getTempoTotalEp() {
		return tempoTotalEp;
	}
	public void setTempoTotalEp(String tempoTotalEp) {
		this.tempoTotalEp = tempoTotalEp;
	}
	public String getCodigoEp() {
		return codigoEp;
	}
	public void setCodigoEp(String codigoEp) {
		this.codigoEp = codigoEp;
	}
	
	
}

	