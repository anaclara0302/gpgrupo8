package view;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.SystemColor;


public class TelaPrincipalAdmGUI extends JFrame {

	private JPanel contentPane;

	//Lista de funcionarios

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipalAdmGUI frame = new TelaPrincipalAdmGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	//hhhgjhsjhgsb
	/**
	 * Create the frame.
	 */
	public TelaPrincipalAdmGUI() {
		setTitle("Projeto S\u00E9ries");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 497, 366);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnCadastrar = new JMenu("Cadastrar");
		mnCadastrar.setForeground(new Color(255, 69, 0));
		menuBar.add(mnCadastrar);

		JMenuItem mnSerie = new JMenuItem("S\u00E9rie");
		mnSerie.setForeground(SystemColor.activeCaption);
		mnSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {


				CadastroSerieGUI serie = new CadastroSerieGUI();

				serie.setVisible(true);


			}
		});
		mnCadastrar.add(mnSerie);

		JMenuItem mnTemporada = new JMenuItem("Temporada");
		mnTemporada.setForeground(SystemColor.activeCaption);
		mnTemporada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				CadastroTemporadaGUI temporada = new CadastroTemporadaGUI();

				temporada.setVisible(true);



			}
		});
		mnCadastrar.add(mnTemporada);

		JMenuItem mnEpisódio = new JMenuItem("Epis\u00F3dio");
		mnEpisódio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				CadastroEpisodioGUI ep = new CadastroEpisodioGUI();

				ep.setVisible(true);




			}
		});
JMenuItem mntmEpisdio = new JMenuItem("Epis\u00F3dio");
mntmEpisdio.setForeground(SystemColor.activeCaption);
mntmEpisdio.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent arg0) {
		
		CadastroEpisodioGUI episodio = new CadastroEpisodioGUI();

		episodio.setVisible(true);
	}
});
mnCadastrar.add(mntmEpisdio);



		JMenu mnPesquisar = new JMenu("Pesquisar");
		mnPesquisar.setForeground(new Color(255, 69, 0));
		menuBar.add(mnPesquisar);

		JMenuItem mnPesquisarSerie = new JMenuItem("S\u00E9rie");
		mnPesquisarSerie.setForeground(SystemColor.activeCaption);
		mnPesquisarSerie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				TelaPesquisaSerieGUI pesquisa = new TelaPesquisaSerieGUI();

				pesquisa.setVisible(true);



			}
		});
		mnPesquisar.add(mnPesquisarSerie);
		JMenuItem mnPesquisarTemporada = new JMenuItem("Temporada");
		mnPesquisarTemporada.setForeground(SystemColor.activeCaption);
		mnPesquisarTemporada.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				TelaPesquisaTemporadaGUI pesquisa = new TelaPesquisaTemporadaGUI();

				pesquisa.setVisible(true);
			}


		});
		mnPesquisar.add(mnPesquisarTemporada);
		JMenuItem mnPesquisarEpisodio = new JMenuItem("Epis\u00F3dio");
		mnPesquisarEpisodio.setForeground(SystemColor.activeCaption);
		mnPesquisarEpisodio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {



				TelaPesquisaTemporadaGUI pesquisa = new TelaPesquisaTemporadaGUI();

				pesquisa.setVisible(true);
			}


		});
		mnPesquisar.add(mnPesquisarEpisodio);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
}