package model;

public class ModeloTabelaUsuario {

	private String nomeUsuario;
	private String telefone;
	private String email;
	private String nacionalidade;
	
	
	public ModeloTabelaUsuario() {
		super();
	}
	public ModeloTabelaUsuario(String nomeUsuario, String telefone, String email,
			String nacionalidade) {
		super();
		this.nomeUsuario = nomeUsuario;
		this.telefone = telefone;
		this.email = email;
		this.nacionalidade = nacionalidade;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	
	
	
}
