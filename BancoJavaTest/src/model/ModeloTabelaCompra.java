package model;

public class ModeloTabelaCompra {
	
private String codigo;
private String fornecedor;
private String descricao;
private String tipo;
private Double valor;

public ModeloTabelaCompra(String codigo, String fornecedor, String descricao,
		String tipo, Double valor) {
	super();
	this.codigo = codigo;
	this.fornecedor = fornecedor;
	this.descricao = descricao;
	this.tipo = tipo;
	this.valor = valor;
}
public ModeloTabelaCompra() {
	super();
}
public String getCodigo() {
	return codigo;
}
public void setCodigo(String codigo) {
	this.codigo = codigo;
}
public String getFornecedor() {
	return fornecedor;
}
public void setFornecedor(String fornecedor) {
	this.fornecedor = fornecedor;
}
public String getDescricao() {
	return descricao;
}
public void setDescricao(String descri��o) {
	this.descricao = descri��o;
}
public String getTipo() {
	return tipo;
}
public void setTipo(String tipo) {
	this.tipo = tipo;
}
public Double getValor() {
	return valor;
}
public void setValor(Double valor) {
	this.valor = valor;
}

}
