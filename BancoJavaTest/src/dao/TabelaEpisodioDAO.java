package dao;

import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;

import model.ModeloTabelaEpisodio;




public class TabelaEpisodioDAO {

	
	private ModeloMySql modelo = null;

	public TabelaEpisodioDAO(){

		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("db_projetoSeries");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}


	public boolean Inserir(ModeloTabelaEpisodio episodio){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO episodios (tempoTotalEp, codigoEp ) VALUES(?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			
			statement.setString (1, episodio.getTempoTotalEp());
			statement.setString(2, episodio.getCodigoEp());
		
			

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	

	

	public boolean Alterar(ModeloTabelaEpisodio episodio, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}

		/* prepara o sql para inserir no banco */

		String sql = " UPDATE episodios SET tempoTotalEp =?, codigoEp = ? WHERE codigoEp = ?;";



		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, episodio.getTempoTotalEp());
			statement.setString (2, episodio.getCodigoEp());
			
			
			statement.setString(3, key);
			
			System.out.println(statement);
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String codigoEp) {
		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		String sql = "DELETE FROM episodios where codigoEp = ?;";
		

		try {
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1,codigoEp);

			statement.executeUpdate();
			status = true;



		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return status;

	}public ArrayList<ModeloTabelaEpisodio> Listar(){
		
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaEpisodio> episodio = new ArrayList<ModeloTabelaEpisodio>();
		
		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}
		
		
		/* prepara o sql para selecionar no banco */
		
		String sql = "SELECT * FROM episodios;";
		
		/* prepara para selecionar no banco de dados */
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			/* recebe os resultados da query */
			while(result.next()){
				
			
				ModeloTabelaEpisodio episodios = new ModeloTabelaEpisodio();
				episodios.setTempoTotalEp(result.getString("tempoTotal"));
				episodios.setCodigoEp(result.getString("codigoEp"));		
				
				episodio.add(episodios);
				
			}
						
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		
		}
		
		return episodio;
	}
}



