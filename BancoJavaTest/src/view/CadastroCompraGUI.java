package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import model.ModeloTabelaCliente;
import model.ModeloTabelaCompra;
import dao.CompraDAO;
import dao.TabelaClienteDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextArea;

public class CadastroCompraGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtCodigo;
	private JTextField txtDescricao;
	private JTextField txtFornecedor;
	private JTextField txtTipo;
	private JTextField txtValor;
	private JLabel lblCdigoAntigo;
	private JTextField txtCodigoAntigo;
	private JButton btnAlterar;
	private JButton btnExcluir;
	private JTable table;
	private JButton btnPesquisar;
	private JScrollPane scrollPane_1;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroCompraGUI frame = new CadastroCompraGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroCompraGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 486, 469);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(75, 0, 130));
		contentPane.setForeground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setForeground(new Color(255, 255, 255));
		lblCdigo.setBounds(35, 26, 46, 14);
		contentPane.add(lblCdigo);
		
		JLabel lblDescrio = new JLabel("Descri\u00E7\u00E3o:");
		lblDescrio.setForeground(new Color(255, 255, 255));
		lblDescrio.setBounds(22, 51, 59, 14);
		contentPane.add(lblDescrio);
		
		JLabel lblFornecedor = new JLabel("Fornecedor:");
		lblFornecedor.setForeground(new Color(255, 255, 255));
		lblFornecedor.setBounds(12, 76, 69, 14);
		contentPane.add(lblFornecedor);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setForeground(new Color(255, 255, 255));
		lblTipo.setBounds(47, 107, 30, 14);
		contentPane.add(lblTipo);
		
		JLabel lblValor = new JLabel("Valor:");
		lblValor.setForeground(new Color(255, 255, 255));
		lblValor.setBounds(226, 26, 35, 14);
		contentPane.add(lblValor);
		
		txtCodigo = new JTextField();
		txtCodigo.setBounds(91, 23, 125, 20);
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);
		
		txtDescricao = new JTextField();
		txtDescricao.setColumns(10);
		txtDescricao.setBounds(91, 48, 315, 20);
		contentPane.add(txtDescricao);
		
		txtFornecedor = new JTextField();
		txtFornecedor.setColumns(10);
		txtFornecedor.setBounds(91, 73, 315, 20);
		contentPane.add(txtFornecedor);
		
		txtTipo = new JTextField();
		txtTipo.setColumns(10);
		txtTipo.setBounds(91, 104, 315, 20);
		contentPane.add(txtTipo);
		
		txtValor = new JTextField();
		txtValor.setColumns(10);
		txtValor.setBounds(266, 23, 140, 20);
		contentPane.add(txtValor);
		
		JButton btnInserir = new JButton("Inserir");
		btnInserir.setBackground(new Color(255, 255, 255));
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				/* recupera os dados da interface gráfica */
				String codigo = txtCodigo.getText();
				String descri�ao = txtDescricao.getText();
				String fornecedor = txtFornecedor.getText();
				String tipo = txtTipo.getText();
				Double valor = Double.parseDouble(txtValor.getText());
				
				
				/* cria um objeto para encapsular os campos */
				ModeloTabelaCompra compra = new ModeloTabelaCompra(codigo,descri�ao,fornecedor,tipo,valor);


				/* cria o objeto de dao */
				CompraDAO daoCompra = new CompraDAO();

				if(daoCompra.Inserir(compra)){

					JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
				} else {
					JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
				}
				
				
				
			}
		});
		btnInserir.setBounds(190, 135, 89, 23);
		contentPane.add(btnInserir);
		
		lblCdigoAntigo = new JLabel("C\u00F3digo antigo");
		lblCdigoAntigo.setForeground(new Color(255, 255, 255));
		lblCdigoAntigo.setBounds(12, 172, 79, 14);
		contentPane.add(lblCdigoAntigo);
		
		txtCodigoAntigo = new JTextField();
		txtCodigoAntigo.setBounds(91, 169, 147, 20);
		contentPane.add(txtCodigoAntigo);
		txtCodigoAntigo.setColumns(10);
		
		btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				
				String codigo = txtCodigo.getText();
				String descricao = txtDescricao.getText();
				String fornecedor = txtFornecedor.getText();
				String tipo = txtTipo.getText();
				Double valor = Double.parseDouble(txtValor.getText());
				String CodigoAntigo = txtCodigoAntigo.getText();
				

				/* cria um objeto para encapsular os campos */
				ModeloTabelaCompra compra = new ModeloTabelaCompra(codigo,descricao,fornecedor,tipo,valor);



				
				/* cria o objeto de dao */
				CompraDAO daoCompra = new CompraDAO();

				if(daoCompra.Alterar(compra, CodigoAntigo)){

					JOptionPane.showMessageDialog(null, "alterado com sucesso!!");
				} else {
					JOptionPane.showMessageDialog(null, "n�o foi posss�vel alterar!!");
				}
				
				
		
			}
		});
		btnAlterar.setBounds(248, 169, 79, 23);
		contentPane.add(btnAlterar);
		
		btnExcluir = new JButton("excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CompraDAO daoCompra = new CompraDAO();
				String codigo = txtCodigo.getText();
				
				if(daoCompra.Delete(codigo)){
					JOptionPane.showMessageDialog(null, "deletado com sucesso!!");
				}
				else{
					JOptionPane.showMessageDialog(null, "n�o foi posss�vel deletar!!");
					
				}
				
			}
		});
		btnExcluir.setBounds(337, 169, 79, 23);
		contentPane.add(btnExcluir);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 274, 315, 85);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"codigo", "descri\u00E7\u00E3o", "fornecedor", "tipo", "valor"
			}
		));
		scrollPane.setViewportView(table);
		
		btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			
		public void actionPerformed(ActionEvent e) {
			
			DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
			ArrayList<String> row = new ArrayList<String>();

			modelTable.setNumRows(0);

			CompraDAO daoCompra= new CompraDAO();

			ArrayList<ModeloTabelaCompra> produto = daoCompra.Listar();

			for(ModeloTabelaCompra a: produto){

				row.add(a.getCodigo());
				row.add(a.getDescricao());
				row.add(a.getFornecedor());
				row.add(a.getTipo());
				row.add(String.valueOf(a.getValor()));


				modelTable.addRow(row.toArray());
				row.clear();

			}
			
			
			
			}
		});
		btnPesquisar.setBounds(337, 336, 107, 23);
		contentPane.add(btnPesquisar);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(22, 221, 313, 23);
		contentPane.add(scrollPane_1);
		
		final JTextArea textArea = new JTextArea();
		scrollPane_1.setViewportView(textArea);
		
		btnNewButton = new JButton("Persquisar por:");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();

				modelTable.setNumRows(0);

				CompraDAO daoCompra= new CompraDAO();

				ArrayList<ModeloTabelaCompra> compra = daoCompra.Listar();

				String pesquisarPor = textArea.getText();

				for(ModeloTabelaCompra a : compra){
					if(a.getCodigo().equals(pesquisarPor)){
						row.add(a.getCodigo());
						row.add(a.getDescricao());
						row.add(a.getFornecedor());
						row.add(a.getTipo());
						row.add(String.valueOf(a.getValor()));

						modelTable.addRow(row.toArray());
						row.clear();

					}
				}
				
				
			}
		});
		btnNewButton.setBounds(343, 221, 117, 23);
		contentPane.add(btnNewButton);
	}
}
