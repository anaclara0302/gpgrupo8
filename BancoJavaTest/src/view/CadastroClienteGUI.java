package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



import javax.swing.table.DefaultTableModel;

import controller.FabricaDeConexao;
import dao.TabelaClienteDAO;
import model.ModeloMySql;
import model.ModeloTabelaCliente;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JTable;
import javax.swing.JScrollPane;

public class CadastroClienteGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEndereco;
	private JTextField txtIdade;
	private JTextField txtNomeAntigo;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroClienteGUI frame = new CadastroClienteGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroClienteGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 431, 492);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 228, 225));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(12, 41, 70, 15);
		contentPane.add(lblNome);

		txtNome = new JTextField();
		txtNome.setBounds(12, 56, 286, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);

		JLabel lblEndereo = new JLabel("Endere\u00E7o:");
		lblEndereo.setBounds(12, 87, 176, 15);
		contentPane.add(lblEndereo);

		txtEndereco = new JTextField();
		txtEndereco.setBounds(12, 102, 389, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);

		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(317, 41, 70, 15);
		contentPane.add(lblIdade);

		txtIdade = new JTextField();
		txtIdade.setBounds(317, 56, 84, 20);
		contentPane.add(txtIdade);
		txtIdade.setColumns(10);

		JButton btnGravar = new JButton("Inserir");
		btnGravar.setBackground(SystemColor.menu);
		btnGravar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				/* recupera os dados da interface gráfica */
				String nome = txtNome.getText();
				String endereco = txtEndereco.getText();
				int idade = Integer.parseInt(txtIdade.getText());


				/* cria um objeto para encapsular os campos */
				ModeloTabelaCliente cliente = new ModeloTabelaCliente(nome,endereco,idade);


				/* cria o objeto de dao */
				TabelaClienteDAO daoCliente = new TabelaClienteDAO();

				if(daoCliente.Inserir(cliente)){

					JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
				} else {
					JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
				}
				
			}
		});
		
		btnGravar.setBounds(228, 133, 84, 24);
		contentPane.add(btnGravar);
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.setBackground(SystemColor.menu);
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				

				/* recupera os dados da interface gráfica */
				String nome = txtNome.getText();
				String endereco = txtEndereco.getText();
				int idade = Integer.parseInt(txtIdade.getText());
				String nomeAntigo = txtNomeAntigo.getText();
				
				


				/* cria um objeto para encapsular os campos */
				ModeloTabelaCliente cliente = new ModeloTabelaCliente(nome,endereco,idade);


				/* cria o objeto de dao */
				TabelaClienteDAO daoCliente = new TabelaClienteDAO();
				
				
				if(daoCliente.Alterar(cliente, nomeAntigo)){
					JOptionPane.showMessageDialog(null, "alterado com sucesso!!");
				}
				else{
					JOptionPane.showMessageDialog(null, "n�o foi posss�vel alterar!!");
					
				}
					
			}
		});
		btnAlterar.setBounds(10, 201, 84, 24);
		contentPane.add(btnAlterar);
		
		JLabel lblNomeAntigo = new JLabel("Nome antigo:");
		lblNomeAntigo.setBackground(new Color(0, 0, 0));
		lblNomeAntigo.setBounds(12, 150, 84, 14);
		contentPane.add(lblNomeAntigo);
		
		txtNomeAntigo = new JTextField();
		txtNomeAntigo.setBounds(10, 175, 391, 20);
		contentPane.add(txtNomeAntigo);
		txtNomeAntigo.setColumns(10);
		
		JLabel lblCadastroCliente = new JLabel("Cadastro Cliente");
		lblCadastroCliente.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCadastroCliente.setBounds(12, 11, 144, 14);
		contentPane.add(lblCadastroCliente);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				TabelaClienteDAO daoCliente = new TabelaClienteDAO();
				String nome = txtNome.getText();
				
				if(daoCliente.Delete(nome)){
					JOptionPane.showMessageDialog(null, "deletado com sucesso!!");
				}
				else{
					JOptionPane.showMessageDialog(null, "n�o foi posss�vel deletar!!");
					
				}
				
			}
		});
		btnExcluir.setBackground(SystemColor.menu);
		btnExcluir.setBounds(317, 133, 84, 24);
		contentPane.add(btnExcluir);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 268, 389, 161);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nome", "Endereco", "Idade"
			}
		));
		scrollPane.setViewportView(table);
		
		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();
				
				modelTable.setNumRows(0);
				
				TabelaClienteDAO daoCliente= new TabelaClienteDAO();
				
				ArrayList<ModeloTabelaCliente> clientes = daoCliente.Listar();
				
				for(ModeloTabelaCliente a: clientes){

					row.add(a.getNomeCliente());
					row.add(a.getEnderecoCliente());
					row.add(String.valueOf(a.getIdadeCliente()));
					modelTable.addRow(row.toArray());
					row.clear();
					
				}
				
		
			
				
				
			}
		});
		btnPesquisar.setBackground(SystemColor.menu);
		btnPesquisar.setBounds(301, 234, 100, 23);
		contentPane.add(btnPesquisar);
	}
}
