package dao;

import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.ModeloTabelaSerie;
import model.ModeloTabelaTemporada;




public class TabelaTemporadaDAO {

	
	private ModeloMySql modelo = null;

	public TabelaTemporadaDAO(){

		modelo = new ModeloMySql();

		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("db_projetoSeries");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}


	public boolean Inserir(ModeloTabelaTemporada temporada){


		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}


		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO temporada (tempototal, codigo ) VALUES(?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			
			statement.setString (1, temporada.getTempoTemporada());
			statement.setString(2, temporada.getCodigo());
			
			
			

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	

	

	public boolean Alterar(ModeloTabelaTemporada temporada, String key){

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}

		/* prepara o sql para inserir no banco */

		String sql = " UPDATE temporada SET tempototal =?, codigo = ? WHERE codigo =?;";



		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */

			statement.setString (1, temporada.getTempoTemporada());
			statement.setString(2, temporada.getCodigo());
			
			
			statement.setString(3, key);
			
			System.out.println(statement);
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}

		return status;

	}

	public boolean Delete(String codigo) {
		
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conexão ao banco");
			return false;
		}
		String sql = "DELETE FROM temporada where codigo = ?;";
		

		try {
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1,codigo);

			statement.executeUpdate();
			status = true;



		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);

		}
		return status;

	}public ArrayList<ModeloTabelaTemporada> Listar(){
		
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* vetor para armazenar os livros obtidos */
		ArrayList<ModeloTabelaTemporada> temporada = new ArrayList<ModeloTabelaTemporada>();
		
		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return null;
		}
		
		
		/* prepara o sql para selecionar no banco */
		
		String sql = "SELECT * FROM temporada;";
		
		/* prepara para selecionar no banco de dados */
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			
			/* recebe os resultados da query */
			while(result.next()){
				
			
				ModeloTabelaTemporada temporadas = new ModeloTabelaTemporada();
				temporadas.setCodigo(result.getString("codigo") );
				temporadas.setTempoTemporada(result.getString("tempoTemporada"));
					
				
				temporada.add(temporadas);
				
			}
						
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao 
			FabricaDeConexao.closeConnection(con);
		
		}
		
		return temporada;
	}
}



