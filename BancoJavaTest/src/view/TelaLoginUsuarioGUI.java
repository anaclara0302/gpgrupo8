package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JCheckBox;

import java.awt.Panel;

import javax.swing.SwingConstants;

import java.awt.Label;

import javax.swing.ImageIcon;

import dao.TabelaLoginUserDAO;

import javax.swing.JPasswordField;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;



public class TelaLoginUsuarioGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtLoginUser;
	private JPasswordField txtSenhaUser;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaLoginUsuarioGUI frame = new TelaLoginUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaLoginUsuarioGUI() {
		setForeground(Color.BLACK);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 673, 541);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 69, 0));
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtLoginUser = new JTextField();
		txtLoginUser.setBackground(new Color(255, 255, 255));
		txtLoginUser.setBounds(374, 252, 201, 29);
		contentPane.add(txtLoginUser);
		txtLoginUser.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblSenha.setForeground(new Color(70,130,180));
		lblSenha.setBounds(374, 301, 46, 14);
		contentPane.add(lblSenha);
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnEnter.setForeground(new Color(70,130,180));
		btnEnter.setBackground(new Color(255,250,250));
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Logar();
			}
		});
		btnEnter.setBounds(486, 366, 89, 23);
		contentPane.add(btnEnter);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Lembre-se de mim neste aparelho ");
		chckbxNewCheckBox.setForeground(new Color(255, 69, 0));
		chckbxNewCheckBox.setBackground(Color.BLACK);
		chckbxNewCheckBox.setBounds(373, 397, 227, 24);
		contentPane.add(chckbxNewCheckBox);
		
		JLabel lblCadastrese = new JLabel("Cadastre-se");
		lblCadastrese.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				CadastroUsuarioGUI cadastro =new CadastroUsuarioGUI();
				
				

				cadastro.setVisible(true);
			}
		});
		lblCadastrese.setForeground(new Color(255, 69, 0));
		lblCadastrese.setBounds(374, 428, 97, 23);
		contentPane.add(lblCadastrese);
		
		JLabel lblEsqueceuASenha = new JLabel("Esqueceu a senha ");
		lblEsqueceuASenha.setForeground(new Color(255, 69, 0));
		lblEsqueceuASenha.setBounds(374, 456, 157, 14);
		contentPane.add(lblEsqueceuASenha);
		
		JLabel lblLogin = new JLabel("Login:");
		lblLogin.setBounds(374, 225, 76, 16);
		contentPane.add(lblLogin);
		lblLogin.setHorizontalAlignment(SwingConstants.LEFT);
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblLogin.setForeground(new Color(70,130,180));
		
		JLabel label = new JLabel("");
		label.setBounds(158, 250, 46, 14);
		contentPane.add(label);
		
		txtSenhaUser = new JPasswordField();
		txtSenhaUser.setBounds(374, 326, 201, 29);
		contentPane.add(txtSenhaUser);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\51r1hGBUFDL._AC_SX60_CR,0,0,60,60_.jpg"));
		lblNewLabel.setBounds(0, 0, 60, 60);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\60x60-jJU.jpg"));
		lblNewLabel_2.setBounds(60, 0, 60, 60);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\565cf732b846b185448fa882_60.jpg"));
		lblNewLabel_3.setBounds(120, 0, 60, 60);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("New label");
		lblNewLabel_4.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\5332294.jpg"));
		lblNewLabel_4.setBounds(180, 0, 60, 60);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("New label");
		lblNewLabel_5.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\9939886.jpg"));
		lblNewLabel_5.setBounds(240, 0, 60, 60);
		contentPane.add(lblNewLabel_5);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\arrow-beauty-and-the-beast-emily-owens-md-poster-slice.jpg"));
		label_1.setBounds(300, 0, 60, 60);
		contentPane.add(label_1);
		
		JLabel label_6 = new JLabel("New label");
		label_6.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\Black-Mirror-2013-102x150-1-60x60.jpg"));
		label_6.setBounds(360, 0, 60, 60);
		contentPane.add(label_6);
		
		JLabel label_9 = new JLabel("New label");
		label_9.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\breaking-bad-tv-programs-photo-u7.jpg"));
		label_9.setBounds(420, 0, 60, 60);
		contentPane.add(label_9);
		
		JLabel label_16 = new JLabel("New label");
		label_16.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\download (1).jpg"));
		label_16.setBounds(480, 0, 60, 60);
		contentPane.add(label_16);
		
		JLabel label_17 = new JLabel("New label");
		label_17.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\download (3).jpg"));
		label_17.setBounds(540, 0, 60, 60);
		contentPane.add(label_17);
		
		JLabel label_18 = new JLabel("New label");
		label_18.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\download (2).jpg"));
		label_18.setBounds(600, 0, 60, 60);
		contentPane.add(label_18);
		
		JLabel label_2 = new JLabel("New label");
		label_2.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\download (5).jpg"));
		label_2.setBounds(0, 60, 60, 60);
		contentPane.add(label_2);
		
		JLabel label_5 = new JLabel("New label");
		label_5.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\download.png"));
		label_5.setBounds(60, 60, 60, 60);
		contentPane.add(label_5);
		
		JLabel label_8 = new JLabel("New label");
		label_8.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\Grimm-2015-60x60.jpg"));
		label_8.setBounds(120, 60, 60, 60);
		contentPane.add(label_8);
		
		JLabel label_13 = new JLabel("New label");
		label_13.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\download (4).jpg"));
		label_13.setBounds(180, 60, 60, 60);
		contentPane.add(label_13);
		
		JLabel label_14 = new JLabel("New label");
		label_14.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\download.jpg"));
		label_14.setBounds(240, 60, 60, 60);
		contentPane.add(label_14);
		
		JLabel label_15 = new JLabel("New label");
		label_15.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\60x60-5Xu.jpg"));
		label_15.setBounds(300, 60, 60, 60);
		contentPane.add(label_15);
		
		JLabel label_22 = new JLabel("New label");
		label_22.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\jessica-jones-poster-reviews-e1477320470517.jpg"));
		label_22.setBounds(360, 60, 60, 60);
		contentPane.add(label_22);
		
		JLabel label_23 = new JLabel("New label");
		label_23.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\Walking_Dead_S2_Poster.jpg"));
		label_23.setBounds(420, 60, 60, 60);
		contentPane.add(label_23);
		
		JLabel label_24 = new JLabel("New label");
		label_24.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\best-of-friends-podcastx60.jpg"));
		label_24.setBounds(480, 60, 60, 60);
		contentPane.add(label_24);
		
		JLabel label_25 = new JLabel("New label");
		label_25.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\house-of-cards.jpg"));
		label_25.setBounds(540, 60, 60, 60);
		contentPane.add(label_25);
		
		JLabel label_26 = new JLabel("New label");
		label_26.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\Homeland-2012-60x60.jpg"));
		label_26.setBounds(600, 60, 60, 60);
		contentPane.add(label_26);
		
		JLabel label_3 = new JLabel("New label");
		label_3.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\how-i-met-your-mother.jpg"));
		label_3.setBounds(0, 120, 60, 60);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("New label");
		label_4.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\images.jpg"));
		label_4.setBounds(60, 120, 60, 60);
		contentPane.add(label_4);
		
		JLabel label_7 = new JLabel("New label");
		label_7.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\mr-robot.jpg"));
		label_7.setBounds(120, 120, 60, 60);
		contentPane.add(label_7);
		
		JLabel label_12 = new JLabel("New label");
		label_12.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\orphan-black-1a-temporada_t67865_2.png"));
		label_12.setBounds(180, 120, 60, 60);
		contentPane.add(label_12);
		
		JLabel label_11 = new JLabel("New label");
		label_11.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\photo.jpg"));
		label_11.setBounds(300, 120, 60, 60);
		contentPane.add(label_11);
		
		JLabel label_10 = new JLabel("New label");
		label_10.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\scream-queens-1a-temporada_t97900.png"));
		label_10.setBounds(240, 120, 60, 60);
		contentPane.add(label_10);
		
		JLabel label_21 = new JLabel("New label");
		label_21.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\prison-break-header-60x60_c.jpg"));
		label_21.setBounds(360, 120, 60, 60);
		contentPane.add(label_21);
		
		JLabel label_20 = new JLabel("New label");
		label_20.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\UFC205StrangerThings-60x60.jpg"));
		label_20.setBounds(420, 120, 60, 60);
		contentPane.add(label_20);
		
		JLabel label_19 = new JLabel("New label");
		label_19.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\True-Blood-2008-60x60.jpg"));
		label_19.setBounds(480, 120, 60, 60);
		contentPane.add(label_19);
		
		JLabel label_27 = new JLabel("New label");
		label_27.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\the-vampire-diaries-izle-60x60.jpg"));
		label_27.setBounds(540, 120, 60, 60);
		contentPane.add(label_27);
		
		JLabel label_28 = new JLabel("New label");
		label_28.setIcon(new ImageIcon("C:\\Users\\14151000030\\Pictures\\S\u00E9ries\\Teen Wolf Season 1 Torrent Download.jpg"));
		label_28.setBounds(600, 120, 60, 60);
		contentPane.add(label_28);
		
		JLabel lblSeriosonit = new JLabel("SERIESONIT");
		lblSeriosonit.setFont(new Font("Buxton Sketch", Font.PLAIN, 70));
		lblSeriosonit.setForeground(new Color(255, 69, 0));
		lblSeriosonit.setBounds(26, 252, 339, 115);
		contentPane.add(lblSeriosonit);
	}
	private void Logar(){
		
		TabelaLoginUserDAO l = new TabelaLoginUserDAO();
		
		String login, senha;
		
		login = txtLoginUser.getText();
		senha = new String(txtSenhaUser.getPassword());
		
		if(l.Validar(login, senha)){
			
			//InicioGUI telaInicio = new InicioGUI();
			//telaInicio.setVisible(true);
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						WorkspaceUsuarioGUI frame = new WorkspaceUsuarioGUI();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			this.dispose();
		}
			
	}
}
