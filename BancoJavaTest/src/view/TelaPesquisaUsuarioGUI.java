package view;
//OI
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;

import javax.swing.JLabel;

import model.ModeloTabelaEpisodio;
import model.ModeloTabelaUsuario;
import model.ModeloTabelaUsuario;
import dao.TabelaEpisodioDAO;
import dao.TabelaSerieDAO;
import dao.TabelaUsuarioDAO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.awt.SystemColor;
import java.awt.Font;


public class TelaPesquisaUsuarioGUI extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPesquisaTemporadaGUI frame = new TelaPesquisaTemporadaGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPesquisaUsuarioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 586, 427);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 69, 0));
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {


				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();

				modelTable.setNumRows(0);

				TabelaUsuarioDAO daoTabelaUsuario= new TabelaUsuarioDAO();

				ArrayList<ModeloTabelaUsuario> usuario = daoTabelaUsuario.Listar();

				for(ModeloTabelaUsuario a: usuario){

					row.add(a.getNomeUsuario());
					row.add(a.getTelefone());
					row.add(a.getEmail());
					row.add(a.getNacionalidade());
					

					modelTable.addRow(row.toArray());
					row.clear();
				};	
			} 
		});
		btnPesquisar.setForeground(new Color(70,130,180));
		btnPesquisar.setBackground(new Color(230, 230, 250));
		btnPesquisar.setBounds(238, 342, 89, 23);
		contentPane.add(btnPesquisar);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(192, 32, 311, 33);
		contentPane.add(scrollPane);
		final JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setBackground(new Color(255, 255, 255));
		JButton btnPesquisarPor = new JButton("Pesquisar por...");
		btnPesquisarPor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DefaultTableModel modelTable=(DefaultTableModel)table.getModel();
				ArrayList<String> row = new ArrayList<String>();

				modelTable.setNumRows(0);

				TabelaUsuarioDAO daoTabelaUsuario= new TabelaUsuarioDAO();

				ArrayList<ModeloTabelaUsuario> TabelaUsuarioDAO = daoTabelaUsuario.Listar();

				String pesquisar = textArea.getText();

				for(ModeloTabelaUsuario a : TabelaUsuarioDAO){
					if(a.getNomeUsuario().equals(pesquisar)){
						row.add(a.getNomeUsuario());
						row.add(a.getTelefone());
						row.add(a.getEmail());
						row.add(a.getNacionalidade());
						
					
						modelTable.addRow(row.toArray());
						row.clear();					
					}
				}
			}});
		btnPesquisarPor.setForeground(new Color(70,130,180));
		btnPesquisarPor.setBackground(new Color(230, 230, 250));
		btnPesquisarPor.setBounds(230, 89, 109, 23);
		contentPane.add(btnPesquisarPor);
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(26, 136, 477, 195);
		contentPane.add(scrollPane_1);
		table = new JTable();
		table.setBackground(new Color(176, 196, 222));
		table.setEnabled(false);
		table.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"NomeUsuario", "Telefone", "Email", "Nacionalidade"
				}
				));
		scrollPane_1.setViewportView(table);
		JLabel lblNewLabel = new JLabel("Nome usuario:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel.setForeground(new Color(255, 69, 0));
		lblNewLabel.setBackground(new Color(0, 0, 0));
		lblNewLabel.setBounds(29, 38, 119, 14);
		contentPane.add(lblNewLabel);
		
		JButton button = new JButton("Voltar");
		button.setForeground(new Color(70,130,180));
		button.setBounds(471, 354, 89, 23);
		contentPane.add(button);
	}
	/* comentario de teste*/
}

