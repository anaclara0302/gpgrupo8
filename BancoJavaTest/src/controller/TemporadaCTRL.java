package controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.ModeloTabelaSerie;
import model.ModeloTabelaTemporada;
import view.CadastroSerieGUI;
import view.CadastroTemporadaGUI;
import dao.TabelaSerieDAO;
import dao.TabelaTemporadaDAO;

public class TemporadaCTRL {
	
	public void salvar(String tempoTemporada, String codigo) {
		ModeloTabelaTemporada temporada = new ModeloTabelaTemporada(tempoTemporada, codigo);
		
		TabelaTemporadaDAO daoTemporada = new TabelaTemporadaDAO();
		//chama DAO
		if(daoTemporada.Inserir(temporada)){
			JOptionPane.showMessageDialog(null,"sucesso", "gravado.",JOptionPane.INFORMATION_MESSAGE);
		}else{
			JOptionPane.showMessageDialog(null,"Erro", "Problemas ao gravar.",JOptionPane.ERROR_MESSAGE);
		}
		
	}

	public ArrayList<ModeloTabelaTemporada> getTemporada() {
		ArrayList<ModeloTabelaTemporada> temporada = null;
		TabelaTemporadaDAO daoTemporada = new TabelaTemporadaDAO();
		temporada = daoTemporada.Listar();
		return temporada;
	}

	public void deletarTemporada(String codigo) {
		int opcao = JOptionPane.CANCEL_OPTION; // Por padr�o assumimos como se o
		// usu�rio tivesse cancelado a
		// opera��o
		ArrayList<ModeloTabelaTemporada> temporada = getTemporada();
		if (temporada != null) {
			for (int i = 0; i < temporada.size(); i++) {
				String cod = temporada.get(i).getCodigo();
				
				if (cod.equals(codigo) ) {
					opcao = JOptionPane.showConfirmDialog(null,
							"<html>Deseja deletar a temporada:<br>cod.: " + cod + "<br>\"  ?</html>");
					break;
				}
			}
			if (opcao == JOptionPane.YES_OPTION) {
				TabelaTemporadaDAO daoTemporada = new TabelaTemporadaDAO();
				if(codigo != null && codigo.equals("")){
					//chama DAO
					daoTemporada.Delete(" Codigo = '"+codigo+"'");
				}
				JOptionPane.showMessageDialog(null, "Temporada deletada");
			}
		}
	}

	public void configuraSenha(char[] password) {
		new String(password);
		JOptionPane.showMessageDialog(null, "Senha salva para esta sess�o");
	}
	
	public TableModel getTableModel(String codigo) {
		ArrayList<ModeloTabelaTemporada> temporada = getTemporada();
		CadastroTemporadaGUI tela = new CadastroTemporadaGUI();
		if (temporada != null) {
			DefaultTableModel model = tela.modelTable();
			for (int i = 0; i < temporada.size(); i++) {
				String cod = temporada.get(i).getCodigo();
				String tempoTemporada = temporada.get(i).getTempoTemporada();
				
				
				if (cod.equals(codigo) ) {
					Object[] row ={tempoTemporada,codigo}  ;
					model.addRow(row);
					break;
				}
			}
			
			
			
			
			return model;
		} else {
			JOptionPane.showMessageDialog(null, "N�o h� temporadas para listar");
			return null;
		}
	}


	public TableModel getTableModel() {
		ArrayList<ModeloTabelaTemporada> temporada = getTemporada();
		CadastroTemporadaGUI tela = new CadastroTemporadaGUI();
		if (temporada != null) {
			DefaultTableModel model = tela.modelTable();
			
			for (int i = 0; i < temporada.size(); i++) {
				String codigo = temporada.get(i).getCodigo();
				String tempoTemporada = temporada.get(i).getTempoTemporada();
				
				Object[] row ={tempoTemporada, codigo }  ;
				model.addRow(row);
			}
			return model;
		} else {
			JOptionPane.showMessageDialog(null, "N�o h� temporadas para listar");
			return null;
		}
	}

}
